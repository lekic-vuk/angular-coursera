import { Component, OnInit, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { Promotion } from '../shared/promotion';
import { PromotionService } from '../services/promotion.service';
import { Leader } from '../shared/leader';
import { LeaderService } from '../services/leader.service';

import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class HomeComponent implements OnInit {

  dish: Dish;
  dishErrorMessage: string;
  promotionErrorMessage: string;
  leaderErrorMessage: string;
  promotion: Promotion;
  leader: Leader;

  constructor(private dishService: DishService,
              private promotionService: PromotionService,
              private leaderService: LeaderService,
              @Inject('BaseURL') private baseURL) { }

  ngOnInit() {
    this.dishService.getFeaturedDish()
                    .subscribe((x) => this.dish = x,
                                (errorMessage) => this.dishErrorMessage = <any>errorMessage);
    this.promotionService.getFeaturedPromotion()
                          .subscribe((x) => this.promotion = x,
                                      (errorMessage) => this.promotionErrorMessage = <any>errorMessage);
    this.leaderService.getFeaturedLeader()
                      .subscribe((x) => this.leader = x,
                                  (errorMessage) => this.leaderErrorMessage = <any>errorMessage);
  }

}
