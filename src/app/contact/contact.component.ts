import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Feedback, ContactType } from '../shared/feedback';

import { FeedbackService } from '../services/feedback.service';

import { flyInOut, flyOut, flyIn, expand } from '../animations/app.animation';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block'
  },
  animations: [
    flyInOut(),
    flyIn(),
    flyOut(),
    expand()
  ]
})
export class ContactComponent implements OnInit {

  feedbackForm: FormGroup;
  readonly feedbackFormInitial: FormGroup;
  @ViewChild('fform') feedbackFormDirective;

  feedback: Feedback;
  contactType = ContactType;

  foo = new Option()

  formSubmitting = false;
  timeout = false;

  formErrors = {
    'firstname': '',
    'lastname': '',
    'telnum': '',
    'email': ''
  }

  validationMessages = {
    'firstname': {
      'required':  'First name is required.',
      'minlength': 'First name must be at least 2 characters long.',
      'maxlength': 'First name cannot be more than 25 characters long.'
    },
    'lastname': {
      'required':  'Last name is required.',
      'minlength': 'Last name must be at least 2 characters long.',
      'maxlength': 'Last name cannot be more than 25 characters long.'
    },
    'telnum': {
      'required':  'Telephone number is required.',
      'pattern':   'Telephone number cannot contain letters.'
    },
    'email': {
      'required':  'E-mail is required.',
      'pattern':   'E-mail not in valid format.'
    }
  }

  constructor(private fb: FormBuilder,
              private feedbackService: FeedbackService) {
    this.createForm();
    this.feedbackFormInitial = this.feedbackForm;
  }

  private createForm() {
    this.feedbackForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      lastname: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      telnum: ['', [Validators.required, Validators.pattern]],
      email: ['', [Validators.required, Validators.email]],
      agree: false,
      contacttype: 'None',
      message: ''
    });
    this.feedbackForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    if (!this.feedbackForm) { return; }
    const form = this.feedbackForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  ngOnInit() {
    this.feedback = null;
  }

  onSubmit() {
    this.formSubmitting = true;
    this.feedbackService.submitFeedback(this.feedbackForm.value)
                        .subscribe(feedback => {
                                    this.feedback = feedback; this.formSubmitting = false; this.timeout = true;
                                    setTimeout(() => {this.timeout = false; }, 5000);
                                  },
                                  errmsg => { this.feedback = null; this.formSubmitting = false; console.log(<any>errmsg); });

    this.feedbackFormDirective.resetForm();
    this.feedbackForm.reset(this.feedbackFormInitial);
    
  }

  get diagnostic() { return JSON.stringify(this.feedbackForm.value); }

}
