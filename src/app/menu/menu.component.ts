import { Component, OnInit, Inject } from '@angular/core';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';

import { flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})

export class MenuComponent implements OnInit {

  dishes: Dish[];
  menuErrorMessage: string;

  constructor(private dishService: DishService,
              @Inject('BaseURL') private baseURL) {
  }

  ngOnInit() {
    this.dishService.getDishes().subscribe((dishes) => this.dishes = dishes,
                                            (errorMessage) => this.menuErrorMessage = <any>errorMessage);
  }

  onPut() {
    console.log("oin put");
    let dish:Dish =  new Dish();
      dish = {
        id: '1',
        name: 'ffffff',
        image: '/assets/images/Uthappizza.png',
        category: 'mains',
        featured: true,
        label: 'Hot',
        price: '4.99',
        // tslint:disable-next-line:max-line-length
        description: 'teeeeeeeeeeeeeeeeeeeeeeeeeeeest',
        comments: [
            {
                rating: 2,
                comment: 'Imagine!',
                author: 'baa',
                date: '2012-10-16T17:57:28.556094Z'
            }
        ]
    
    };
    this.dishService.putDish(dish)
                    .subscribe(dish => {console.log("done!"); console.log(dish); });
  }

}
