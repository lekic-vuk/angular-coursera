import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MatSliderModule } from '@angular/material/slider';

import { Dish } from '../shared/dish';
import { DISHES } from '../shared/dishes';
import { DishService } from '../services/dish.service';
import { Comment } from '../shared/comment';

import { switchMap } from 'rxjs/operators';
import { visibility, flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block'
  },
  animations: [
    visibility(),
    flyInOut(),
    expand()
  ]
})
export class DishdetailComponent implements OnInit {

  dish: Dish;
  dishcopy: Dish;
  prevID: string;
  nextID: string;
  dishIDs: string[];

  commentForm: FormGroup;
  readonly commentFormInitial: FormGroup;
  comment: Comment;

  visibility = 'shown';

  dishErrorMessage: string;

  @ViewChild('commentform') commentFormDirective;

  formErrors = {
    'author': '',
    'rating': '',
    'comment': ''
  }

  validationMessages = {
    'author': {
      'required':  'Author name is required',
      'minlength': 'Author name must be at least 2 characters long'
    },
    'rating': {
      '':  ''
    },
    'comment': {
      'required':  'Comment is required'
    }
  }

  constructor(private dishService: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder,
    @Inject('BaseURL') private baseURL) {
      this.commentFormInitial = this.initForm();
      this.commentForm = this.commentFormInitial;
      this.onValueChanged();
  }

  private initForm(): FormGroup {
    let form = this.fb.group({
      rating: [5],
      comment: ['', [Validators.required] ],
      author: ['', [Validators.required, Validators.minLength(2)] ],
      date: ''
    });
    form.valueChanges.subscribe(data => this.onValueChanged(data));
    return form;
  }

  onValueChanged(data?: any) {
    if (!this.commentForm) { return; }
    const form = this.commentForm;
    for (const error in this.formErrors) {
      if (this.formErrors.hasOwnProperty(error)) {
        this.formErrors[error] = '';
        const control = form.get(error);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[error];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[error] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  ngOnInit() {
    this.dishService.getDishIDs()
                    .subscribe((x) => this.dishIDs = x);
    this.route.params
              .pipe(switchMap((params: Params) =>
                              { this.visibility = 'hidden'; return this.dishService.getDish(params.id) }))
              .subscribe((dish) => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
                         (errorMessage) => this.dishErrorMessage = <any>errorMessage);
  }

  setPrevNext(dishID: string) {
    const ids = this.dishIDs;
    const size = ids.length;
    const index = ids.indexOf(dishID);

    this.prevID = ids[(size + index - 1) % size];
    this.nextID = ids[(size + index + 1) % size];
  }

  goBack(): void {
    this.location.back();
  }

  onSubmit() {
    const date = new Date().toISOString();
    this.comment = this.commentForm.value;
    this.comment.date = date;

    this.dishcopy.comments.push(this.comment);
    this.dishService.putDish(this.dishcopy)
                    .subscribe(dish => {
                        this.dish = dish;
                        this.dishcopy = dish;
                        console.log("=== SUBSCRIBE OBSERVER RESPONDED ===");
                        console.log(this.dish);
                        console.log(this.dishcopy);
                        console.log("====}============");
                    },
                               errmsg => { this.dish = null; this.dishcopy = null; this.dishErrorMessage = <any>errmsg } );
    console.log("===ON SUBMIT ===");
    console.log(this.dish);
    console.log(this.dishcopy);
    console.log("================");

    this.commentFormDirective.resetForm();
    this.commentForm.reset({
      rating: 5,
      comment: '',
      author: ''
    });
  }

  preview() { if (this.commentForm.valid) {
    return JSON.stringify(this.commentForm.value);
    } else return null;
  }

}
