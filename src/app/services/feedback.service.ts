import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';

import { ProcessHTTPMessageService } from './process-httpmessage.service';

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { Feedback } from '../shared/feedback';
import { baseURL } from '../shared/baseurl';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  private readonly myURL: string
  private readonly httpOptions = {
	headers: new HttpHeaders({
    	'Content-Type': 'application/json'
  	})
  };

  constructor(private http: HttpClient,
  				private processHTTPMessageService: ProcessHTTPMessageService) {
  	this.myURL = baseURL + 'feedback/';
  }

  getFeedbacks(): Observable<Feedback[]> {
  	return this.http.get<Feedback[]>(this.myURL, this.httpOptions)
  					.pipe(catchError(this.processHTTPMessageService.handleError));
  }

  getFeedback(id: string): Observable<Feedback> {
    return this.http.get<Feedback>(this.myURL + id)
                    .pipe(catchError(this.processHTTPMessageService.handleError));
  }

  submitFeedback(feedback: Feedback): Observable<Feedback> {
  	return this.http.post<Feedback>(this.myURL, feedback, this.httpOptions)
  					.pipe(catchError(this.processHTTPMessageService.handleError));
  }
  
}
