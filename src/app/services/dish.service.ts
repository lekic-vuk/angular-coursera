import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Dish } from '../shared/dish';
import { baseURL } from '../shared/baseurl';

import { ProcessHTTPMessageService } from './process-httpmessage.service';

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DishService {

  private readonly myURL: string;
  private readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private http: HttpClient,
              private processHTTPMessageService: ProcessHTTPMessageService) {
    this.myURL = baseURL + 'dishes/';
  }

  getDishes(): Observable<Dish[]> {
    return this.http.get<Dish[]>(this.myURL, this.httpOptions)
                    .pipe(catchError(this.processHTTPMessageService.handleError));
  }

  getDish(id: string): Observable<Dish> {
    return this.http.get<Dish>(this.myURL + id, this.httpOptions)
                    .pipe(catchError(this.processHTTPMessageService.handleError));
  }

  getFeaturedDish(): Observable<Dish> {
    return this.http.get<Dish[]>(this.myURL + '?featured=true', this.httpOptions)
                    .pipe(map(dishes => dishes[0]))
                    .pipe(catchError(this.processHTTPMessageService.handleError));
  }

  getDishIDs(): Observable<string[] | any> {
    return this.http.get<Dish[]>(this.myURL, this.httpOptions)
                    .pipe(map(dishes => dishes.map(dish => dish.id)))
                    .pipe(catchError(this.processHTTPMessageService.handleError));
  }

  putDish(dish: Dish): Observable<Dish> {
    return this.http.put<Dish>(this.myURL + dish.id, dish, this.httpOptions)
                    .pipe(catchError(this.processHTTPMessageService.handleError));
  }

}