import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Leader } from '../shared/leader';
import { LEADERS } from '../shared/leaders';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { baseURL } from '../shared/baseurl';

@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  private readonly myURL: string;

  constructor(private http: HttpClient) {
    this.myURL = baseURL + "leadership/"
  }

  getLeaders(): Observable<Leader[]> {
    return this.http.get<Leader[]>(this.myURL);
  }

  getLeader(id: string): Observable<Leader> {
    return this.http.get<Leader>(this.myURL + id)
  }

  getFeaturedLeader(): Observable<Leader> {
    return this.http.get<Leader[]>(this.myURL + '?featured=true')
                    .pipe(map(leaders => leaders[0]));
  }

}
