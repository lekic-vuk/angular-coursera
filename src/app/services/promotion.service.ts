import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Promotion } from '../shared/promotion';

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { baseURL } from '../shared/baseurl';

@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  private readonly myURL: string;

  constructor(private http: HttpClient) {
    this.myURL = baseURL + "promotions";
  }

  getPromotions(): Observable<Promotion[]> {
    return this.http.get<Promotion[]>(this.myURL);
  }

  getPromotion(id: string): Observable<Promotion> {
    return this.http.get<Promotion>(this.myURL + id);
  }

  getFeaturedPromotion(): Observable<Promotion> {
    return this.http.get<Promotion[]>(this.myURL + "?featured=true")
                    .pipe(map(promotions => promotions[0]));
  }

}
