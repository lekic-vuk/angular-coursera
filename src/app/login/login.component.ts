import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef} from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material';
import { MatFormFieldModule} from '@angular/material/form-field';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

	user = { username: '', password: '', remember: false}
	
  	constructor(public dialogRef: MatDialogRef<LoginComponent>) { }

  	ngOnInit() {
  	}

  	onSubmit() {
  		console.log('Login!');
  		console.log('User: ', this.diagnostic);
  		this.dialogRef.close();
  	}

  	get diagnostic() { return JSON.stringify(this.user); }

}
